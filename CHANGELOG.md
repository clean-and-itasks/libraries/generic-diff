# Changelog

#### 1.0.3

- Chore: accept `base` `3.0`.

#### 1.0.2

- Chore: accept text and containers 2.0.

#### 1.0.1

- Chore: update generic-print dependency.

## 1.0.0

- Initial version, import modules from clean platform v0.3.38 and destill all
  generic diffing modules.
