# generic-diff

This library provides generic machinery to perform diffing between types
generically.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
